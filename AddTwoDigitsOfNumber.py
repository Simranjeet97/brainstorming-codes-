def adddigits(n):
	return sum([int(elem) for elem in list(str(n))])